package org.petstore.models.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "customer_order")
public class CustomerOrder implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "order_id")
    private int OrderId;

    @Column(name = "company_name")
    private String CompanyName;

    @Column(name = "animal_id")
    private int AnimalId;

    @Column(name = "sale_price")
    private Double salePrice;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    public Status getStatus() {
        return status;
    }

    public enum Status {
        InProgress,
        Closed
    }

    @Column(name = "status")
    private Status status;

    public CustomerOrder() { }

    @JsonSetter("order_id")
    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    @JsonSetter
    public void setStatus(Status stat) {
        status = stat;
    }

    @JsonGetter("order_id")
    public int getOrderId() {
        return OrderId;
    }

    @JsonGetter("company_name")
    public String getCompanyName() {
        return CompanyName;
    }

    @JsonSetter("company_name")
    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    @JsonGetter("animal_id")
    public int getAnimalId() {
        return AnimalId;
    }

    @JsonSetter("animal_id")
    public void setAnimalId(int animalId) {
        AnimalId = animalId;
    }

    @JsonGetter("sale_price")
    public Double getSalePrice() {
        return salePrice;
    }

    @JsonSetter("sale_price")
    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

}

