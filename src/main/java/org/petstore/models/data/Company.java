package org.petstore.models.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "company")
public class Company implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "company_id")
    private int id;

    @Column(name = "company_name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "business_number")
    private String businessNumber;

    public Company() {}

    public void setName(String name) {
        this.name = name;
    }

    @JsonGetter("company_name")
    public String getName() {
        return this.name;
    }

    @JsonGetter("company_id")
    public int getId() {
        return id;
    }

    @JsonSetter("company_id")
    public void setId(int companyId) {
        this.id = companyId;
    }

    @JsonGetter("address")
    public String getAddress() {
        return address;
    }

    @JsonSetter("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonGetter("business_number")
    public String getBusinessNumber() {
        return businessNumber;
    }

    @JsonSetter("business_number")
    public void setBusinessNumber(String businessNumber) {
        this.businessNumber = businessNumber;
    }
}
