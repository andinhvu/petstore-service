package org.petstore.models.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "animal")
public class Animal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "animal_id")
    private int animalId;

    @Column(name = "animal_name")
    private String name;

    @Column(name = "dob")
    private Date dob;

    @Column(name = "animal_breed")
    private String breed;

    @Column(name = "animal_type")
    @Enumerated(EnumType.STRING)
    private AnimalType type;

    @Column(name = "purchase_price")
    private double purchasePrice;

    @Column(name = "sale_price")
    private double salePrice;

    public Animal() {
    }

    @JsonSetter("animal_name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonGetter("animal_name")
    public String getName() {
        return this.name;
    }

    @JsonSetter("dob")
    public void setDob(Date dob) {
        this.dob = dob;
    }

    @JsonGetter("dob")
    public Date getDob() {
        return this.dob;
    }

    @JsonSetter("animal_id")
    public void setAnimalId(Integer id) {
        this.animalId = id;
    }

    @JsonGetter("animal_id")
    public Integer getAnimalId() {
        return animalId;
    }

    @JsonGetter("type")
    public AnimalType getType() {
        return type;
    }

    @JsonSetter("type")
    public void setType(AnimalType type) {
        this.type = type;
    }

    @JsonGetter("purchase_price")
    public double getPurchasePrice() {
        return purchasePrice;
    }

    @JsonSetter("purchase_price")
    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    @JsonGetter("sales_price")
    public double getSalePrice() {
        return salePrice;
    }

    @JsonSetter("sale_price")
    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    @JsonGetter("breed")
    public String getBreed() {
        return breed;
    }

    @JsonSetter("breed")
    public void setBreed(String breed) {
        this.breed = breed;
    }

    public enum AnimalType {
        dog,
        cat,
        fish,
        bird
    }
}