package org.petstore.models.data;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "purchase_order")
public class PurchaseOrder implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "order_id")
    private int OrderId;

    @Column(name = "company_name")
    private String CompanyName;

    @Column(name = "animal_id")
    private String AnimalId;

    @Column(name = "sale_price")
    private Double salePrice;

    public PurchaseOrder() { }

    @JsonSetter("order_id")
    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    @JsonGetter("order_id")
    public int getOrderId() {
        return OrderId;
    }

    @JsonGetter("company_name")
    public String getCompanyName() {
        return CompanyName;
    }

    @JsonSetter("company_name")
    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    @JsonGetter("animal_id")
    public String getAnimalId() {
        return AnimalId;
    }

    @JsonSetter("animal_id")
    public void setAnimalId(String animal) {
        AnimalId = animal;
    }

    @JsonGetter("sale_price")
    public Double getSalePrice() {
        return salePrice;
    }

    @JsonSetter("sale_price")
    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }
}
