package org.petstore.models.dao;

public class DAOFactory {
    public static AnimalDao getAnimalDao() {
        return new AnimalDaoImp();
    }

    public static CustomerOrderDao getCustomerOrderDao() {
        return new CustomerOrderDaoImp();
    }

    public static PurchaseOrderDao getPurchaseOrderDao() {
        return new PurchaseOrderDaoImp();
    }

    public static CompanyDao getCompanyDao() {
        return new CompanyDaoImp();
    }
}