package org.petstore.models.dao;

import org.petstore.HibernateUtil;
import org.petstore.models.data.CustomerOrder;

public class CustomerOrderDaoImp implements CustomerOrderDao {
    private String domainName = "CustomerOrder";
    private final HibernateUtil util = new HibernateUtil();

    @Override
    public int createCustomerOrder(CustomerOrder order) {
        util.create(order, domainName);
        return order.getOrderId();
    }

    @Override
    public void updateCustomerOrder(CustomerOrder order) {
        util.update(order, domainName);
    }

    @Override
    public CustomerOrder getCustomerOrder(int id) {
        return (CustomerOrder) util.get(id, CustomerOrder.class);
    }

    @Override
    public void deleteCustomerOrder(CustomerOrder order) {
        util.delete(order, domainName);
    }
}
