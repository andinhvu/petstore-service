package org.petstore.models.dao;

import org.petstore.models.data.PurchaseOrder;

public interface PurchaseOrderDao {
    public int createPurchaseOrder(PurchaseOrder order);

    public void updatePurchaseOrder(PurchaseOrder order);

    public PurchaseOrder getPurchaseOrder(int id);

    public void deletePurchaseOrder(PurchaseOrder order);
}
