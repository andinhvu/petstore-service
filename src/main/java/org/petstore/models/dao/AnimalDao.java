package org.petstore.models.dao;

import org.petstore.models.data.Animal;

import java.util.List;

public interface AnimalDao {

    public int createAnimal(Animal animal);

    public AnimalDao updateAnimal(Animal animal);

    public Animal getAnimal(int id);

    public void deleteAnimal(Animal animal);

    public List<Animal> searchAnimal(String criteria);

}
