package org.petstore.models.dao;

import org.petstore.models.data.CustomerOrder;

public interface CustomerOrderDao {
    public int createCustomerOrder(CustomerOrder order);

    public void updateCustomerOrder(CustomerOrder order);

    public CustomerOrder getCustomerOrder(int id);

    public void deleteCustomerOrder(CustomerOrder order);
}
