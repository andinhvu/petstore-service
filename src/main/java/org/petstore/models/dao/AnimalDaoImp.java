package org.petstore.models.dao;

import org.petstore.HibernateUtil;
import org.petstore.models.data.Animal;

import java.util.List;

public class AnimalDaoImp implements AnimalDao {
    private String domainName = "Animal";
    private final HibernateUtil util = new HibernateUtil();

    @Override
    public int createAnimal(Animal animal) {
        util.create(animal, domainName);
        return animal.getAnimalId();
    }

    @Override
    public AnimalDao updateAnimal(Animal animal) {
        util.update(animal, domainName);
        return null;
    }

    @Override
    public Animal getAnimal(int id) {
        Object animal = util.get(id, Animal.class);
        return (Animal) animal;
    }

    @Override
    public void deleteAnimal(Animal animal) {
        util.delete(animal, domainName);
    }

    @Override
    public List searchAnimal(String criteria) {
        return util.search(criteria);
    }
}
