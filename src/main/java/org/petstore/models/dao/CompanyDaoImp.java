package org.petstore.models.dao;

import org.petstore.HibernateUtil;
import org.petstore.models.data.Company;

public class CompanyDaoImp implements CompanyDao {
    private String domainName = "Company";
    private final HibernateUtil util = new HibernateUtil();

    @Override
    public int createCompany(Company company) {
        util.create(company, domainName);
        return company.getId();
    }

    @Override
    public void updateCompany(Company company) {
        util.update(company, domainName);
    }

    @Override
    public Company getCompany(int id) {
        return (Company) util.get(id, Company.class);
    }

    @Override
    public void deleteCompany(Company company) {
        util.delete(company, domainName);
    }
}
