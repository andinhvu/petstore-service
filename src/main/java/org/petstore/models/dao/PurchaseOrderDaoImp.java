package org.petstore.models.dao;

import org.petstore.HibernateUtil;
import org.petstore.models.data.PurchaseOrder;

public class PurchaseOrderDaoImp implements PurchaseOrderDao {
    private String domainName = "PurchaseOrder";
    private final HibernateUtil util = new HibernateUtil();

    @Override
    public int createPurchaseOrder(PurchaseOrder order) {
        util.create(order, domainName);
        return order.getOrderId();
    }

    @Override
    public void updatePurchaseOrder(PurchaseOrder order) {
        util.update(order, domainName);
    }

    @Override
    public PurchaseOrder getPurchaseOrder(int id) {
        return (PurchaseOrder) util.get(id, PurchaseOrder.class);
    }

    @Override
    public void deletePurchaseOrder(PurchaseOrder order) {
        util.delete(order, domainName);
    }
}
