package org.petstore.models.dao;

import org.petstore.models.data.Company;

public interface CompanyDao {
    public int createCompany(Company company);

    public void updateCompany(Company company);

    public Company getCompany(int id);

    public void deleteCompany(Company company);
}
