package org.petstore.controller;

import org.petstore.models.dao.CompanyDao;
import org.petstore.models.dao.DAOFactory;
import org.petstore.models.data.Company;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1")
public class CompanyController {

    @PostMapping("/company")
    public Company createCompany(@RequestBody Company company) {
        CompanyDao dao = DAOFactory.getCompanyDao();
        int companyId = dao.createCompany(company);
        company.setId(companyId);
        return company;
    }

    @GetMapping(value = "/company/{companyID}",
    produces = MediaType.APPLICATION_JSON_VALUE)
    public Company getCompany( @PathVariable int companyID ){
        Company company = DAOFactory.getCompanyDao().getCompany(companyID);
        return company;
    }
}
