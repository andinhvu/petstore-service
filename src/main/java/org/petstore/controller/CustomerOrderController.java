package org.petstore.controller;

import org.petstore.models.dao.DAOFactory;
import org.petstore.models.data.CustomerOrder;
import org.petstore.models.dao.CustomerOrderDao;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1")
public class CustomerOrderController {

    /**
     * Create order
     * @return
     */
    @PostMapping("/order")
    @ResponseStatus(HttpStatus.CREATED)
    public CustomerOrder createOrder(@RequestBody CustomerOrder order) {
        CustomerOrderDao dao = DAOFactory.getCustomerOrderDao();
        int orderID = dao.createCustomerOrder(order);
        order.setOrderId(orderID);
        return order;
    }

    @GetMapping(name = "/order/{orderId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomerOrder getOrder(@PathVariable int orderId) {
        return DAOFactory.getCustomerOrderDao().getCustomerOrder(orderId);
    }

    /**
     * Delete will happen only if InProgress Status and exists
     * @param orderId
     */
    @DeleteMapping("/order/{orderId}")
    public void deleteOrder(@PathVariable int orderId) {
        deleteProcess(orderId);
    }

    private void deleteProcess(int orderId) {
        CustomerOrderDao dao = DAOFactory.getCustomerOrderDao();
        CustomerOrder order = dao.getCustomerOrder(orderId);
        if (order != null && order.getStatus() == CustomerOrder.Status.InProgress) {
            dao.deleteCustomerOrder(order);
            System.out.println("OrderId " + order.getOrderId() + " has been deleted");
        } else
            if(order.getStatus() == CustomerOrder.Status.Closed) {
                System.out.println("Order already closed cannot cancel.");
            }
            else {
                System.out.println("Order does not exist");
            }
    }
}