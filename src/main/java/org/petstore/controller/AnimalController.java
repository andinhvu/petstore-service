package org.petstore.controller;

import org.hibernate.annotations.Parameter;
import org.petstore.models.dao.AnimalDao;
import org.petstore.models.dao.DAOFactory;
import org.petstore.models.data.Animal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class AnimalController {


    /**
     * Post API to create an animal for the database.
     *
     * This is a internal API.
     *
     * @param animal
     * @return
     */
    @PostMapping("/animal")
    public Animal createAnimal(@RequestBody Animal animal) {
        int id = DAOFactory.getAnimalDao().createAnimal(animal);
        animal.setAnimalId(id);
        return animal;
    }

    /**
     * Get API to return an animal based on ID.
     *
     * @param id
     * @return
     */
    @GetMapping("/animal/{animalId}")
    public Animal getAnimal(@PathVariable int id) {
        Animal animal = DAOFactory.getAnimalDao().getAnimal(id);
        return animal;
    }

    /**
     * Searching based on type of animal.
     *
     * @param type
     * @return
     */
    @GetMapping("/animal/search")
    public java.util.List searchAnimal(@RequestParam("type") String type) {
        List<Animal> returnSearch = DAOFactory.getAnimalDao().searchAnimal(type);
        return returnSearch;
    }

    /**
     * Allows for changes to the animal database.
     *
     * This is for Intenral
     * @param animalId
     * @param animal
     * @return
     */
    @PutMapping("/animal/{animalId}")
    public Animal ammendAnimal(
            @PathVariable int animalId,
            @RequestBody Animal animal
    ) {
        animal.setAnimalId(animalId);
        DAOFactory.getAnimalDao().updateAnimal(animal);
        return animal;
    }

    @DeleteMapping("/animal/{animalId}")
    public void deleteAnimal(@PathVariable int animalId) {
        AnimalDao dao = DAOFactory.getAnimalDao();
        Animal animal = dao.getAnimal(animalId);
        if (animal != null) {
            dao.deleteAnimal(animal);
        }
        else {
            System.out.println("Animal not found exist to be deleted");
        }
    }
}
