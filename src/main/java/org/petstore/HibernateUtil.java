package org.petstore;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.petstore.models.data.Animal;

import java.util.Collection;
import java.util.List;

public class HibernateUtil {

    private Session currentSession;

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            Configuration configuration = new Configuration();
            return configuration.buildSessionFactory(new StandardServiceRegistryBuilder()
                    .configure("hibernate.cfg.xml")
                    .build());
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error building the factory");
        }
    }

    public Session openCurrentSession() {
        currentSession = getSessionFactory().openSession();
        return currentSession;
    }

    public void closeCurrentSessionWithTransaction(Transaction currentTransaction) {
        if (currentSession != null && currentTransaction != null) {
            currentTransaction.commit();
            currentSession.close();
        }
    }

    private static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void create(Object object, String entity) {
        Transaction currentTransaction = null;
        if (currentSession == null || !currentSession.isOpen()) currentSession = openCurrentSession();
        try {
            currentTransaction = currentSession.beginTransaction();
            currentSession.save(entity, object);
        } catch (Exception e) {
            if (currentTransaction != null) {
                currentTransaction.rollback();
            }
        } finally {
            closeCurrentSessionWithTransaction(currentTransaction);
        }
    }
    @SuppressWarnings("rawtypes")
    public Object get(int id, Class entity) {
        Transaction currentTransaction;
        if (currentSession == null || !currentSession.isOpen()) currentSession = openCurrentSession();
        currentTransaction = currentSession.beginTransaction();
        Object domainEntity = currentSession.get(entity, id);
        closeCurrentSessionWithTransaction(currentTransaction);
        return domainEntity;
    }

    public void update(Object object, String entity) {
        Transaction currentTransaction = null;
        try {
            if (currentSession == null || !currentSession.isOpen()) currentSession = openCurrentSession();
            currentTransaction = currentSession.beginTransaction();
            currentSession.update(entity, object);
        } catch (Exception e) {
            if (currentTransaction != null) {
                currentTransaction.rollback();
            }
        } finally {
            closeCurrentSessionWithTransaction(currentTransaction);
        }
    }

    public void delete(Object object, String entity) {
        Transaction currentTransaction = null;
        try {
            if (currentSession == null || !currentSession.isOpen()) currentSession = openCurrentSession();
            currentTransaction = currentSession.beginTransaction();
            currentSession.delete(entity, object);
        } catch (Exception e) {
            if (currentTransaction != null) {
                currentTransaction.rollback();
            }
        } finally {
            closeCurrentSessionWithTransaction(currentTransaction);
        }
    }

    public List search(String criteria) {
        Transaction currentTransaction = null;
        List<Animal> listOfAnimals = null;
        try {
            if (currentSession == null || !currentSession.isOpen()) currentSession = openCurrentSession();
            currentTransaction = currentSession.beginTransaction();
            Query query = currentSession.createQuery("FROM Animal t WHERE t.name = 'Porkie'");
            listOfAnimals = query.list();
            for (Animal t : listOfAnimals) {
                System.out.println(t.getAnimalId());
            }
            currentTransaction.commit();
        } catch (Exception e) {
            currentTransaction.rollback();
        }finally{
            currentSession.close();
        }
        return listOfAnimals;
    }
}