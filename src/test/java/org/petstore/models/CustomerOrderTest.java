package org.petstore.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.petstore.models.data.CustomerOrder;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class CustomerOrderTest {

    @Test
    public void checkObjectToJson()
            throws JsonProcessingException {
        CustomerOrder order = new CustomerOrder();
        order.setAnimalId("123");
        order.setCompanyName("client");
        order.setSalePrice(143.00);

        String result = new ObjectMapper().writeValueAsString(order);
        System.out.println(result);
        assertEquals(result, containsString("{\"sale_price\":143.0,\"animal_id\":\"123\",\"company_name\":\"client\",\"order_id\":0}"));
    }

    @Test
    public void checkJsonToObject() throws JsonProcessingException {
        String json = "{\"sale_price\":143.0,\"animal_id\":\"123\",\"company_name\":\"client\",\"order_id\":0}";
        CustomerOrder order = new ObjectMapper().readValue(json,CustomerOrder.class);
        assertEquals(order.getAnimalId(), "123");
        assertEquals(order.getCompanyName(), "client");
    }
}