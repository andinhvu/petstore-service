package org.petstore.service;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.petstore.models.dao.AnimalDao;
import org.petstore.models.dao.CompanyDao;
import org.petstore.models.dao.DAOFactory;
import org.petstore.models.data.Animal;
import org.petstore.models.data.Company;
import org.petstore.models.data.CustomerOrder;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DaoTests {

    private AnimalDao animalDao = DAOFactory.getAnimalDao();
    private CompanyDao companyDao = DAOFactory.getCompanyDao();
    private Date date = new Date(160643752);
    private Animal dog = new Animal();

    @BeforeEach
    private void testSetUp() {
        //dog.setAnimalId(1);
        dog.setType(Animal.AnimalType.dog);
        dog.setName("Porkie");
        dog.setBreed("maltise");
        dog.setDob(date);
    }


    /**
     * Testing the Get of an existing animal
     */
    @Test
    public void animalTest() {
        /**
         * Animal already existing in DB
         * animal_id = 123
         * animal_name = doggy
         * animal_type = dog
         */
        Animal actualDog = animalDao.getAnimal(123);
        assertEquals(actualDog.getType(), Animal.AnimalType.dog);
        assertEquals(actualDog.getName(), "doggy");
    }

    @Test
    public void createAnimalTest() {
        // dog.setAnimalId(70);
        dog.setType(Animal.AnimalType.dog);
        dog.setName("Porkie");
        dog.setBreed("maltise");
        dog.setDob(date);
        int id = animalDao.createAnimal(dog);
        System.out.println(id);
        Animal actualDog = animalDao.getAnimal(id);
        assertEquals(actualDog.getName(), dog.getName());
        animalDao.deleteAnimal(dog);
    }

    /**
     * Testing Create of dog,
     * Update of dog
     * Delete of dog
     */
    @Test
    public void updateAnimalTest() {
        dog.setType(Animal.AnimalType.dog);
        dog.setName("Porkie");
        dog.setBreed("maltise");
        dog.setDob(date);
        int id = animalDao.createAnimal(dog);
        dog.setBreed("Cavoodle");
        animalDao.updateAnimal(dog);
        Animal actualDog = animalDao.getAnimal(id);
        assertEquals(actualDog.getBreed(), dog.getBreed());
        animalDao.deleteAnimal(dog);
    }


    @Test
    public void searchAnimalTest() {
        AnimalDao dao = DAOFactory.getAnimalDao();
        List listOfAnimals = dao.searchAnimal("porkie");
        assertEquals(listOfAnimals.isEmpty(), false);
    }

    @Test
    public void createCompanyTest() {
        Company supplier1 = new Company();
        supplier1.setName("Animals R Us");
        supplier1.setAddress("420 George St");
        int id = companyDao.createCompany(supplier1);
        Company actualCompany = companyDao.getCompany(id);
        assertEquals(actualCompany.getAddress(), supplier1.getAddress());
        assertEquals(actualCompany.getName(), supplier1.getName());
        companyDao.deleteCompany(actualCompany);
    }

    @Test
    public void getCompanyTest() {
        Company company = companyDao.getCompany(160);
        assertEquals(company.getName(), "Breeders R Us");
        assertEquals(company.getAddress(), "400 George St");
    }

    @Test
    public void createCustomerOrderTest() {
        CustomerOrder order = new CustomerOrder();
        order.setAnimalId(120);
    }
}
